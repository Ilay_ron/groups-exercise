#!/usr/bin/env bash

# created by: Ilay ron
# Purpose: Groups exercise
# date: 28/10/2020
# version: v1.0.6
#####################################


Main(){

echo "choose the option that you would like to do: "

select option in Creating_group Adding_a_member_to_group Rename_group Group_management_permission Exit
do
	case $option in
	Creating_groups)
	 Creating_groups
;;
	Adding_a_new_member_to_group)
	Adding_a_member_to_group
;;
	Rename_group)
	 Rename_group
;;
	Group_management_permission)
	Group_management_permission
;;
	Exit)
	break
;;
esac

done

}

Creating_groups(){

echo "please provide the group name that you would like to create: "

read name

for g in $name
do
groupadd $name

done

id

}

Adding_a_member_to_group(){

local gname=group_name
local uname=user_name

echo "please provide the group name: "
read gname
echo "please provide the user name: "
read uname

for name in $uname
do 
usermod -a -G $gname $uname

done


}


Rename_group(){

local gname=group_name
local ngname=new_group_name

echo "choose the group name that you want to change: "
read gname
echo "enter the new name for the group: "
read ngname

for name in $ngname
do
groupmod -n $ngname $gname

done


}

Group_management_permission(){

local $gname=group_name
local $uname=User_name

echo "insert the group name: "
read gname
echo "insert the user name that you want to give admin permissions on the group: "
read uname

for name in $uname
do
gpasswd -A $uname $gname

done
}

Main
